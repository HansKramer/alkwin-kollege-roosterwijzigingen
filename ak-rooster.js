// ==UserScript==
// @name         ak-rooster
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Fix roosterwijzigingen
// @author       You
// @match        http://661203.websites.xs4all.nl/roostertv/lln/subst_*.htm
// @grant        none
// ==/UserScript==

(function() {
    var meta_tags;
    var old_content;

    meta_tags = document.getElementsByTagName('meta');
    for (var i = 0; i < meta_tags.length; i++) {
        var tag = meta_tags[i];

        console.log(tag);
        if (tag.httpEquiv.match(/refresh/i)) {
            if (tag.content.match(/[\D]/)) {
                url = tag.content.match(/url=['"]?([^'"]+)['"]?$/i);
                old_content = tag.content;
                old_content = old_content.split('=')[1];
                //tag.content = "100000; " + window.location;

            }
            //thisMeta.remove();
        }
    }

    var nav_bar = document.createElement("div");

    document.body.prepend(nav_bar);

    var btn = document.createElement("button");
    btn.innerHTML = "Next Page";
    btn.onclick = function() {
        window.location.href = old_content;
    };
    nav_bar.appendChild(btn);

    window.setTimeout(window.stop, 1)
})();
